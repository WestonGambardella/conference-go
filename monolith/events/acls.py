import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_weather(city, state):
    query = f"{city}, {state}, US"
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={query},&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    JSONresponse = json.loads(response.content)

    lat = JSONresponse[0]["lat"]
    lon = JSONresponse[0]["lon"]
    url2 = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    response2 = requests.get(url2)
    JSONresponse2 = json.loads(response2.content)
    weather = {
        "temp": JSONresponse2["main"]["temp"],
        "description": JSONresponse2["weather"][0]["description"],
    }
    return weather


def get_image(city, state):
    payload = {"per_page": 1, "query": f"{city} {state}"}
    headers = {"authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=payload, headers=headers)
    JSONresponse = json.loads(response.content)
    try:
        image = {"picture_url": JSONresponse["photos"][0]["url"]}
        return image
    except KeyError:
        return None
